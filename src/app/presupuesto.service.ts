import { EventEmitter } from '@angular/core';
import { Egreso } from './Egreso.model';
import { Ingreso } from './Ingreso.model';

export class PresupuestoService {
	public egresos: Egreso[] = [];
	public ingresos: Ingreso[] = [];

	constructor() {}

	public addIngreso(ingreso: Ingreso): void {
		this.ingresos.push(ingreso);
	}

	public removeIngreso(index: number): void {
		this.ingresos.splice(index, 1);
	}

	public addEgreso(egreso: Egreso): void {
		this.egresos.push(egreso);
	}

	public removeEgreso(index: number): void {
		this.egresos.splice(index, 1);
	}
}
