import { Component, Input } from '@angular/core';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-ingreso',
	templateUrl: './ingreso.component.html',
	styleUrls: ['./ingreso.component.css']
})
export class IngresoComponent {
	@Input() descripcion!: string;
	@Input() valor!: number;
	@Input() index!: number;

	constructor(private presupuestoService: PresupuestoService) {}

	eliminar(): void {
		this.presupuestoService.removeIngreso(this.index);
	}
}
