import { Component, OnInit } from '@angular/core';
import { Egreso } from '../Egreso.model';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-egresos',
	templateUrl: './egresos.component.html',
	styleUrls: ['./egresos.component.css']
})
export class EgresosComponent implements OnInit {
	public egresos: Egreso[] = [];

	constructor(private presupuestoService: PresupuestoService) {}

	ngOnInit(): void {
		this.egresos = this.presupuestoService.egresos;
	}
}
