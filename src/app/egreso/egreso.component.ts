import { Component, Input, OnInit } from '@angular/core';
import { Ingreso } from '../Ingreso.model';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-egreso',
	templateUrl: './egreso.component.html',
	styleUrls: ['./egreso.component.css']
})
export class EgresoComponent implements OnInit {
	@Input() descripcion!: string;
	@Input() valor!: number;
	@Input() index!: number;

	public ingresos: Ingreso[] = [];

	constructor(private presupuestoService: PresupuestoService) {}

	public ngOnInit(): void {
		this.ingresos = this.presupuestoService.ingresos;
	}

	public eliminar(): void {
		this.presupuestoService.removeEgreso(this.index);
	}

	public getIngreso(): number {
		let total: number;
		this.ingresos.forEach(ingreso => (total += ingreso.valor), (total = 0));
		return total;
	}
}
