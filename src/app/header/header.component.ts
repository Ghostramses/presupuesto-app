import { Component, OnInit } from '@angular/core';
import { Egreso } from '../Egreso.model';
import { Ingreso } from '../Ingreso.model';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	public egresos: Egreso[] = [];
	public ingresos: Ingreso[] = [];

	constructor(private presupuestoService: PresupuestoService) {}

	ngOnInit(): void {
		this.egresos = this.presupuestoService.egresos;
		this.ingresos = this.presupuestoService.ingresos;
	}

	public getIngreso(): number {
		let total: number = 0;
		this.ingresos.forEach(ingreso => (total += ingreso.valor), total);
		return total;
	}

	public getEgreso(): number {
		let total: number = 0;
		this.egresos.forEach(egreso => (total += egreso.valor), total);
		return total;
	}
}
