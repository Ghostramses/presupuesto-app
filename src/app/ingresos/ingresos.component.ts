import { Component, OnInit } from '@angular/core';
import { Ingreso } from '../Ingreso.model';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-ingresos',
	templateUrl: './ingresos.component.html',
	styleUrls: ['./ingresos.component.css']
})
export class IngresosComponent implements OnInit {
	public ingresos: Ingreso[] = [];

	constructor(private presupuestoService: PresupuestoService) {}

	ngOnInit(): void {
		this.ingresos = this.presupuestoService.ingresos;
	}
}
