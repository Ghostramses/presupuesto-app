import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PresupuestoService } from './presupuesto.service';
import { FormularioComponent } from './formulario/formulario.component';
import { FormsModule } from '@angular/forms';
import { IngresosComponent } from './ingresos/ingresos.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { EgresosComponent } from './egresos/egresos.component';
import { EgresoComponent } from './egreso/egreso.component';

@NgModule({
	declarations: [AppComponent, HeaderComponent, FormularioComponent, IngresosComponent, IngresoComponent, EgresosComponent, EgresoComponent],
	imports: [BrowserModule, FormsModule],
	providers: [PresupuestoService],
	bootstrap: [AppComponent]
})
export class AppModule {}
