import { Component } from '@angular/core';
import { Egreso } from '../Egreso.model';
import { Ingreso } from '../Ingreso.model';
import { PresupuestoService } from '../presupuesto.service';

@Component({
	selector: 'app-formulario',
	templateUrl: './formulario.component.html',
	styleUrls: ['./formulario.component.css']
})
export class FormularioComponent {
	public tipoOperacion: string = 'ing';
	public descripcion: string = '';
	public valor: number | null = null;

	constructor(private presupuestoService: PresupuestoService) {}

	public agregar(): void {
		if (this.descripcion && this.valor) {
			switch (this.tipoOperacion) {
				case 'ing':
					this.presupuestoService.addIngreso(
						new Ingreso(this.descripcion, this.valor)
					);
					break;
				case 'egr':
					this.presupuestoService.addEgreso(
						new Egreso(this.descripcion, this.valor)
					);
					break;
			}
			this.descripcion = '';
			this.valor = null;
		}
	}
}
